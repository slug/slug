#!/bin/bash

GLOBIGNORE="common.tex" # don't try to compile common tex files

# automatically cd to root
cd $(git rev-parse --show-toplevel)
# make pdfs of all tex files in slug/docs
echo "Converting docs to pdfs..."
cd docs/
mkdir -p .build
latexmk -quiet -pdf *.tex -output-directory=.build 
mv .build/*.pdf ../pdfs/
echo "doc conversion done."

# make pdfs of all tex files in slug/meetings
echo "Converting meetings to pdfs..."
cd ../meetings/
mkdir -p .build
latexmk -quiet -pdf *.tex -output-directory=.build 
mv .build/*.pdf ../pdfs/
echo "meeting conversion done."
# print that the script is done running
cd ..
#rm -f *.log

cd pdfs
# only generate combined PDF if either have changed
# sue me
git status | grep -q -e "constitution.pdf" -e "bylaws.pdf" && 
pdftk A=./constitution.pdf B=./bylaws.pdf cat A B output constitution-bylaws.pdf

echo "genpdf FINISHED!"
