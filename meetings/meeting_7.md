# starting time: 6pm, online at meet.jit.si/slug
# Intros
* Open meeting w/ introductions for eBoard, then wait until next meeting for in person intros
# History
* What is Linux? What already runs Linux today that you don't know?
* What are distros? (don't spend too much time on this)
# Where should you start?
* Linux Mint
* Ubuntu (preferably a flavor and not the base since
base Ubuntu mangles GNOME so bad)
* elementary
* Desktop environments: just show screenshots of GNOME, KDE, Pantheon, and Xfce. Strengths
and weaknesses of each.
# Do a quick Vim tutorial (actually instead of this, talk about linux news)
* Vim is taught b/c it (or vi) is on every linux system.
* movement w/ vim keys because the mouse is slow
* command vs visual vs insert mode...these are all for the purpose of keeping the
writer's hands on the home row.

# Evan's mirror presentation
* Less technical; letting people know what a mirror is.

# Linux news discussion
* Fortune 500 company used open source software developer Daniel Steinburg's code
to patch their log4j vulnerability and Steinburg demanded payment.
* [source]{https://daniel.haxx.se/blog/2022/01/24/logj4-security-inquiry-response-required/}
